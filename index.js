var d3 = require('d3');
var jsonTree = require('json-tree-viewer');

var CSS_FN = 'style.css';

var N_PRECIS = 3;

d3.select('link#stylesheet')
	.attr('href', CSS_FN);

var net = null;

document.getElementById("net-file")
	.addEventListener("change", function(evt) {
		var files = evt.target.files;
		var reader = new FileReader();

		reader.onload = function(evt) {
			var json = JSON.parse(evt.target.result);
			var map_start = ('map' in json) ? json.map : null;
			if (net !== null)
				net.clean_up();
			net = new Network(d3.select('#network'), d3.select('#properties'),
					map_start);
			net.draw_network(json);
			if (net.map !== null) {
				net.map.position_svg();
				net.map.position_table();
			} else {
				net.centre_view();
			}
			};
		reader.readAsText(files[0]);
		}, false);

document.getElementById("dat-file")
	.addEventListener("change", function(evt) {
		var files = evt.target.files;
		var reader = new FileReader();

		reader.onload = function(evt) {
			net.dhand.data = JSON.parse(evt.target.result);
			net.dhand.ind = 0;
			net.set_data();
			};
		reader.readAsText(files[0]);
		}, false);

document.getElementById("save-network").onclick = function() {
	net.save_network(); // have to wrap because starts off null
	};

window.onkeydown = function(e) {
	// Cross browser support
	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	//console.log(key);
	if (key == 50) { // 2
		net.dhand.decr(1);
		net.set_data();
	} else if (key == 51) { // 3
		net.dhand.incr(1);
		net.set_data();
	} else if (key == 49) { // 1
		net.dhand.decr(20);
		net.set_data();
	} else if (key == 52) { // 4
		net.dhand.incr(20);
		net.set_data();
	}
	};

function DHandler() {
	this.data = null;
	this.ind = 0;
	this.incr = function(val) {
			this.ind = Math.min(this.ind + val, this.data.length - 1);
		};
	this.decr = function(val) {
			this.ind = Math.max(this.ind - val, 0);
		};
	this.get = function() { return this.data[this.ind]; };
}

function Map(cont, map_start) {
	// map zoom is value to set map to after initialisation
	// net zoom is value to initialise map at to give particular network scale
	this.map_zoom = ('map_zoom' in map_start) ? map_start.map_zoom : 16;
	this.net_zoom = ('net_zoom' in map_start) ? map_start.net_zoom : this.map_zoom;
	this.map = L.map(cont.node()).setView(map_start.latlon,
			this.net_zoom);
	this.map.setZoom(this.map_zoom); // now change to actual zoom for user
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(this.map);

	this.overlay = d3.select(this.map.getPanes().overlayPane);
	this.svg = null;
	this.table = null;
	this.table_origin = null;

	this.attach_svg = function(svg) {
		this.svg = svg;
		};

	this.attach_table = function(table) {
		this.table = table;
		this.table_origin = this.map.layerPointToLatLng([0.0, 0.0]);
		};

	this.table_to_geo = function(pos) {
		var scale = this.map.getZoomScale(this.map.getZoom(), this.net_zoom);
		var point = this.map.latLngToContainerPoint(this.table_origin);
		return this.map.containerPointToLatLng([pos[0]*scale + point.x,
				pos[1]*scale + point.y]);
		};

	this.geo_to_table = function(geo) {
		var scale = this.map.getZoomScale(this.map.getZoom(), this.net_zoom);
		var point = this.map.latLngToContainerPoint(this.table_origin);
		var pos = this.map.latLngToContainerPoint(geo);
		pos.x -= point.x;
		pos.y -= point.y;
		pos.x /= scale;
		pos.y /= scale;
		return pos;
		};

	this.position_table = function() {
		var scale = this.map.getZoomScale(this.map.getZoom(), this.net_zoom);
		var point = this.map.latLngToContainerPoint(this.table_origin);
		this.table
			.attr('transform', 'translate(' + point.x + ',' + point.y
						+ ')scale(' + scale + ')');
		};

	this.position_svg = function() {
		var point = this.map.containerPointToLayerPoint([0,0]);
		// Keeping svg locked to window
		this.svg
			.style('left', point.x + 'px')
			.style('top', point.y + 'px');
		};

	this.map.on('dragend', function() {
		this.position_svg();
		this.position_table();
		}, this);

	this.map.on('zoomend', function() {
		this.position_svg();
		this.position_table();
		}, this);

	this.map.on('resize', function() {
		this.position_svg();
		this.position_table();
		}, this);

	this.clean_up = function() {
		this.map.remove();
		};
}

function Network(cont, prop, map_start=null) {
	var that = this;

	this.cont = cont; // viewport container for map/svg with accessible size
	this.wid = cont.node().clientWidth;
	this.hei = cont.node().clientHeight;

	window.onresize = function(evt) {
			that.wid = that.cont.node().clientWidth;
			that.hei = that.cont.node().clientHeight;
			that.svg.attr('width', that.wid)
				.attr('height', that.hei);
		};

	this.prop = prop;

	this.map = null;
	if (map_start !== null)
		this.map = new Map(cont, map_start);
	this.svg_cont = cont;
	if (this.map !== null)
		this.svg_cont = this.map.overlay;
	this.svg = this.svg_cont.append('svg');
	this.svg.attr('width', this.wid)
		.attr('height', this.hei);
	this.table = this.svg.append('g')
		.attr('class', 'table leaflet-zoom-hide');

	if (this.map !== null) {
		this.map.attach_svg(this.svg);
		this.map.attach_table(this.table);
	}

	this.dhand = new DHandler();

	this.defs = this.svg.append('defs');
	this.defs.call(add_markers);

	var node_r = 14;

	var node_drag = d3.behavior.drag()
		.on('dragstart', function(d) {
			d3.event.sourceEvent.stopPropagation();
			})
		.on('drag', function(d) {
			d.xy[0] += d3.event.dx;
			d.xy[1] += d3.event.dy;
			let edge_set = that.node_cons[d.id].edge;
			let chum_set = that.node_cons[d.id].chum;
			d3.select(this)
				.attr('cx', function(d) { return d.xy[0]; })
				.attr('cy', function(d) { return d.xy[1]; });
			that.edge.filter(function(d) { return edge_set.has(d.id); })
				.each(update_edge_pos);
			that.chum.filter(function(d) { return chum_set.has(d.id); })
				.attr('cx', function(d) { return that.nd.comps[d.cons[0]].xy[0] +
					that.chum_offset[d.id].xoff; })
				.attr('cy', function(d) { return that.nd.comps[d.cons[0]].xy[1] +
					that.chum_offset[d.id].yoff; });
			})
		.on('dragend', function(d) {
			});

	this.nd = null; // network data
	this.node_cons = null; // connections for each node
	this.chum_offset = null; // offsets for each chum

	// d3 selections
	this.node = null;
	this.edge = null;
	this.chum = null;

	this.type_to_kind = {};

	this.save_network = function() {
		// Reattached to event so can't use this, use that instead
		Object.keys(this.nd.comps).forEach(function(id) {
			if (!('cons' in this.nd.comps[id])) // no connections so is node
				this.set_node_geo(this.nd.comps[id]);
			}, this);
		var json = JSON.stringify(this.nd);
		var url = 'data:text/json;charset=utf8,' + encodeURIComponent(json);
		//window.open(url, '_blank');
		//window.focus();
		// The above two lines work, but they open in new window.
		// As a workaround create a link, give download attribute and trigger
		// a mouse event on it.
		var link = document.createElement('a');
		link.download = 'network.json';
		link.href = url;
		var click_event = new MouseEvent('click', {
			'view': window,
			'bubbles': true,
			'cancelable': false
			});
		link.dispatchEvent(click_event);
		};

	var update_edge_pos = function(d) {
		// Get node positions
		let x1 = that.nd.comps[d.cons[0]].xy[0];
		let y1 = that.nd.comps[d.cons[0]].xy[1];
		let x2 = that.nd.comps[d.cons[1]].xy[0];
		let y2 = that.nd.comps[d.cons[1]].xy[1];
		let dx = 0.0;
		let dy = 0.0;
		if (x2 == x1) {
			dy = node_r;
		} else {
			var m = Math.abs((y2 - y1)/(x2 - x1));
			dx = Math.sqrt(node_r*node_r/(1 + m*m));
			dy = m*dx;
		}
		dx = x1 <= x2 ? dx : -dx;
		dy = y1 <= y2 ? dy : -dy;
		d3.select(this)
			.attr('x1', x1 + dx)
			.attr('y1', y1 + dy)
			.attr('x2', x2 - dx)
			.attr('y2', y2 - dy);
		};

	var update_positions = function() {
		that.node
			.attr('cx', function(d) { return d.xy[0]; })
			.attr('cy', function(d) { return d.xy[1]; });
		that.chum
			.attr('cx', function(d) { return that.nd.comps[d.cons[0]].xy[0] +
				that.chum_offset[d.id].xoff; })
			.attr('cy', function(d) { return that.nd.comps[d.cons[0]].xy[1] +
				that.chum_offset[d.id].yoff; })
		that.edge
			.each(update_edge_pos);
		};

	var prop_string = function(v) {
		if (Array.isArray(v)) {
			var str = '[';
			for (var i=0; i<v.length; i++) {
				str += prop_string(v[i]);
				if (i < v.length-1) {
					str += ','
				}
			}
			return str + ']';
		} else if (!isNaN(v) && v % 1 !== 0) {
			if (typeof v.toPrecision === 'function')
				return v.toPrecision(4);
			else
				return v.toString();
		} else {
			return v.toString();
		}
		};

	var last_loaded = null;
	var load_prop = function(d) {
		if (d === null)
			return;
		last_loaded = d;
		tprop = document.getElementById('tprop');
		cprop = document.getElementById('cprop');
		vprop = document.getElementById('vprop');
		tprop.innerHTML = ''; // clear all children
		cprop.innerHTML = ''; // clear all children
		vprop.innerHTML = ''; // clear all children
        jsonTree.create(d, tprop);
		if ('cprop' in d) {
            jsonTree.create(d.cprop, cprop);
        }
		if ('vprop' in d) {
            jsonTree.create(d.vprop, vprop);
        }
		};

	var generate_offset = function(mag, i, tot) {
			var ang = Math.PI*(((tot-1)/2.0 - i)/4.0 + 90.0/180.0) ;
			return {"xoff": mag*Math.cos(ang), "yoff": mag*Math.sin(ang)};
		};

	this.set_node_pos = function(n) {
		if (this.map !== null) {
			var point = this.map.geo_to_table(n.latlon);
			n.xy = [point.x, point.y];
		}
		};

	this.set_node_geo = function(n) {
		if (this.map !== null) {
			var geo = this.map.table_to_geo(n.xy);
			n.latlon = [geo.lat, geo.lon];
		}
		};

	this.draw_network = function(nd) {
		this.nd = nd;
		this.node_cons = {};
		this.chum_offset = {};
		let nodes = [];
		let edges = [];
		let chums = [];
		// Need to operate on nodes first to set up node_cons
		Object.keys(nd.comps).forEach(function(id) { // works for array or map
			let c = nd.comps[id];
			c.id = id;
			if (!(c.type in this.type_to_kind))
				this.type_to_kind[c.type] = new Set();
			if (!('cons' in c) || c.cons.length == 0) {
				delete c.cons; // delete if exists
				nodes.push(c);
				this.set_node_pos(c);
				this.node_cons[c.id] = {'edge': new Set(), 'chum': new Set()};
				this.type_to_kind[c.type].add('node');
			}
			}, this);
		Object.keys(nd.comps).forEach(function(id) { // works for array or map
			let c = nd.comps[id];
			if (!('cons' in c) || c.cons.length == 0)
				return;
			if (c.cons.length == 2) { // then edge
				edges.push(c);
				this.node_cons[c.cons[0]].edge.add(c.id);
				this.node_cons[c.cons[1]].edge.add(c.id);
				this.type_to_kind[c.type].add('edge');
			} else if (c.cons.length == 1) { // then chum
				chums.push(c);
				// Temporarily store an index for each chum
				this.chum_offset[c.id] = this.node_cons[c.cons[0]].chum.size;
				this.node_cons[c.cons[0]].chum.add(c.id);
				this.type_to_kind[c.type].add('chum');
			} else {
				console.log('Cannot handle component with ' + c.cons.length
						+ ' connections');
			}
			}, this);
		// Calculate offsets now we know how many per node
		chums.forEach(function(c) {
			this.chum_offset[c.id] = generate_offset(1.5*node_r,
					this.chum_offset[c.id],
					this.node_cons[c.cons[0]].chum.size);
			}, this);
		// If decos doesn't exist in data, add it
		if (!('decos' in nd))
			nd['decos'] = [];
		this.edge = this.table.selectAll('.edge').data(edges);
		this.edge.enter() // create elements when more data than existing
			.append('line')
			.on('click', load_prop)
			.append('title'); // tooltip
		this.edge
			.attr('class', function(d) { return 'edge ' + d.type; })
			.attr('marker-start', function(d) {
				if ('m0' in d)
					return 'url(#sta_' + d.m0 + ')';
				else
					return 'none';
				})
			.attr('marker-end', function(d) {
				if ('m1' in d)
					return 'url(#end_' + d.m1 + ')';
				else
					return 'none';
				});
		this.edge.select('title')
			.attr('name', function(d) { return d.name; })
			.text(function(d) { return d.name; });
		this.edge.exit().remove();

		this.node = this.table.selectAll('.node').data(nodes);
		this.node.enter()
			.append('circle')
			.on('click', load_prop)
			.call(node_drag)
			.append('title'); // tooltip
		this.node
			.attr('class', function(d) { return 'node ' + d.type; })
			.attr('r', node_r);
		this.node.select('title')
			.attr('name', function(d) { return d.name; })
			.text(function(d) { return d.name; });
		this.node.exit().remove();

		this.chum = this.table.selectAll('.chum').data(chums);
		this.chum.enter()
			.append('circle')
			.on('click', load_prop)
			.append('title'); // tooltip
		this.chum
			.attr('class', function(d) { return 'chum ' + d.type; })
			.attr('r', node_r/2);
		this.chum.select('title')
			.attr('name', function(d) { return d.name; })
			.text(function(d) { return d.name; });
		this.chum.exit().remove();

		update_positions();
		this.update_deco('cprop');
		};

	this.update_deco = function(prop) {
		this.nd.decos.forEach(function(d) {
			if (d.prop != prop)
				return;
			switch (d.feat) {
			case 'colour':
				this.type_to_kind[d.type].forEach(function(k) {
					dec_colour(d, k, this[k]);
					}, this);
				break;
			case 'size':
				this.type_to_kind[d.type].forEach(function(k) {
					dec_size(d, k, this[k]);
					}, this);
				break;
			default:
				break;
			}
			}, this);
		};

	this.set_data = function() {
		let dat = this.dhand.get();
		let ind = this.dhand.ind
		this.prop.select('#dataindex').text('Index: ' + ind);

		Object.keys(dat).forEach(function(id) {
			this.nd.comps[id].vprop = dat[id];
			}, this);

		load_prop(last_loaded);
		this.update_deco('vprop');
		};

	// should not be used if map is being used
	this.zoom = d3.behavior.zoom()
		.size([this.wid, this.hei])
		.scaleExtent([0.01, 10])
		.on('zoom', function() {
				that.table.attr('transform', 'translate(' + d3.event.translate +
							')scale(' + d3.event.scale + ')');
			});
	if (this.map === null)
		this.svg.call(this.zoom);

	// should not be used if map is being used
	this.centre_view = function() {
		let xmin = Infinity;
		let ymin = Infinity;
		let xmax = -Infinity;
		let ymax = -Infinity;
		Object.keys(this.nd.comps).forEach(function(id) {
			let c = this.nd.comps[id];
			if ('cons' in c) // only consider nodes
				return;
			xmin = xmin <= c.xy[0] ? xmin : c.xy[0];
			ymin = ymin <= c.xy[1] ? ymin : c.xy[1];
			xmax = xmax >= c.xy[0] ? xmax : c.xy[0];
			ymax = ymax >= c.xy[1] ? ymax : c.xy[1];
			}, this);
		if (xmin == Infinity || ymin == Infinity
				|| xmax == -Infinity || ymax == -Infinity)
			return;
		let xmid = (xmin + xmax)/2;
		let ymid = (ymin + ymax)/2;
		let xscale = this.wid/(xmax - xmin);
		let yscale = this.hei/(ymax - ymin);
		let scale = xscale < yscale ? xscale : yscale;
		let xtran = this.wid/2 - scale*xmid;
		let ytran = this.hei/2 - scale*ymid;
		this.zoom.translate([xtran,ytran]).scale(scale);
		this.zoom.event(this.svg); // call event so gets drawn in new position
		};

	// remove everything in container and reset properties
	this.clean_up = function() {
		if (this.map !== null)
			this.map.clean_up();
		this.cont
			.attr('class', ''); // clear any leaflet classes
		var cont = this.cont.node();
		while (cont.firstChild)
			cont.removeChild(cont.firstChild);

		that.prop.select('#tprop').html('');
		that.prop.select('#cprop').html('');
		that.prop.select('#vprop').html('');
		};
}

function add_markers() {
	this.append('marker')
		.attr('id', 'end_closed')
		.attr('viewBox', '0 0 6 6')
		.attr('refX', 6)
		.attr('refY', 3)
		.attr('markerWidth', 1.5)
		.attr('markerheight', 1.5)
		.attr('orient', 'auto')
		.append('rect')
		.attr('x', 0)
		.attr('y', 0)
		.attr('width', 6)
		.attr('height', 6);
	this.append('marker')
		.attr('id', 'sta_closed')
		.attr('viewBox', '0 0 6 6')
		.attr('refX', 0)
		.attr('refY', 3)
		.attr('markerWidth', 1.5)
		.attr('markerheight', 1.5)
		.attr('orient', 'auto')
		.append('rect')
		.attr('x', 0)
		.attr('y', 0)
		.attr('width', 6)
		.attr('height', 6);
	this.append('marker')
		.attr('id', 'end_open')
		.attr('viewBox', '0 0 6 6')
		.attr('refX', 6)
		.attr('refY', 3)
		.attr('markerWidth', 1.5)
		.attr('markerheight', 1.5)
		.attr('orient', 'auto')
		.append('rect')
		.attr('x', 0)
		.attr('y', 0)
		.attr('width', 6)
		.attr('height', 6);
	this.append('marker')
		.attr('id', 'sta_open')
		.attr('viewBox', '0 0 6 6')
		.attr('refX', 0)
		.attr('refY', 3)
		.attr('markerWidth', 1.5)
		.attr('markerheight', 1.5)
		.attr('orient', 'auto')
		.append('rect')
		.attr('x', 0)
		.attr('y', 0)
		.attr('width', 6)
		.attr('height', 6);
}

function dec_colour(deco, kind, sel) {
	var cfunc = null;
	if ('map' in deco) {
		cfunc = function(v) { return v in deco.map ? deco.map[v] : 'black'; };
	} else {
		var scale = d3.scale.linear()
			.domain('domain' in deco ? deco.domain : [0, 1])
			.range('range' in deco ? deco.range : [0, 270.0])
			.clamp(true);
		cfunc = function(v) { return d3.hsl(scale(v), 0.8, 0.4); };
	}
	sel.filter('.' + deco.type)
		.style(kind == 'edge' ? 'stroke' : 'fill',
			function(d) { return cfunc(d[deco.prop][deco.pname]) });
}

function dec_size(deco, kind, sel) {
	var cfunc = null;
	if ('map' in deco) {
		cfunc = function(v) { return v in deco.map ? deco.map[v] : 8; };
	} else {
		var scale = d3.scale.linear()
			.domain('domain' in deco ? deco.domain : [0, 1])
			.range('range' in deco ? deco.range : [4, 14])
			.clamp(true);
		cfunc = function(v) { return scale(v); };
	}
	if (kind == 'edge') {
		sel.filter('.' + deco.type)
			.style('stroke-width',
				function(d) { return cfunc(d[deco.prop][deco.pname]) });
	} else {
		sel.filter('.' + deco.type)
			.attr('r',
				function(d) { return cfunc(d[deco.prop][deco.pname]) });
	}
}

			//for (var i=0; i<this.dec_inf.length; i++) {
			//	var dc = {};
			//	dc['scale'] = d3.scale.linear();
			//	dc['scale'].domain(this.dec_inf[i]['domain']);
			//	dc['param'] = this.dec_inf[i]['param'];
			//	var par =  this.dec_inf[i]['parent'];
			//	dc['parent'] = par;
			//	var name = par + '_' + this.dec_inf[i]['feat'] + '_' +
			//		this.dec_inf[i]['param'];

			//	switch (this.dec_inf[i]['feat']) {
			//	case 'arc':
			//		dc['func'] = dec_radius;
			//		dc['scale'].range([0, 0.8*node_r]);
			//		var arc = this.table.selectAll('.'+name)
			//			.data(nd[par])
			//			.enter()
			//			.append('path')
			//			.attr('class', name)
			//			.attr('transform', function(d) {
			//					return 'translate(' + that.xt(d.x) + ','
			//							+ that.yt(d.y) + ')';
			//				});
			//		arc.append('title'); // tooltip
			//		dc['obj'] = arc;
			//		break;
			//	case 'text':
			//		dc['func'] = dec_text;
			//		dc['scale'].range([0, 1]);
			//		var txt = this.table.selectAll('.'+name)
			//			.data(nd[par])
			//			.enter()
			//			.append('text')
			//			.attr('class', name)
			//			.attr('x', function(d) {
			//					return that.xt(d.x);
			//				})
			//			.attr('y', function(d) {
			//					return that.yt(d.y);
			//				})
			//			.attr('dx', this.dec_inf[i]['xoff'])
			//			.attr('dy', this.dec_inf[i]['yoff'])
			//			.attr('text-anchor', 'middle');
			//		dc['obj'] = txt;
			//		break;
			//	default:
			//		break;
			//	}
			//	this.dec.push(dc);
			//}
			//
//
//function dec_text(scale, obj, v, dat) {
//	obj.data(dat)
//		.text(function(d) {
//				return scale(d[v].toFixed(N_PRECIS));
//			});
//}

//function plot_data(dat) {
//	var plt1 = new Plot(WIDTH, 200, 0);
//	plt1.addData(dat['resid']['pres'].map(Math.log10), '#885544');
//	plt1.addData(dat['resid']['dres'].map(Math.log10), '#335588');
//	var plt2 = new Plot(WIDTH, 200, 1);
//	plt2.addData(dat['resid']['cost'], '#885544');
//	plt2.addData(dat['resid']['objv'], '#335588');
//}
//
//function Plot(w, h, id) {
//	var x_pad = 50;
//	var y_pad = 25;
//	this.id = id;
//	this.svg = d3.select('body')
//				.append('svg')
//				.attr('width', w)
//				.attr('height', h)
//				.attr('id', id);
//	this.xscale = d3.scale.linear();
//	this.xscale.range([x_pad, w - x_pad]);
//	this.yscale = d3.scale.linear();
//	this.yscale.range([h - y_pad, y_pad]);
//	this.xmax = 0;
//	this.ymin = 0;
//	this.ymax = 0;
//	this.data = [];
//	this.lines = [];
//	this.xaxis = d3.svg.axis()
//			.scale(this.xscale)
//			.orient('bottom')
//			.ticks(5);
//	this.yaxis = d3.svg.axis()
//			.scale(this.yscale)
//			.orient('left')
//			.ticks(5);
//	this.gxaxis = this.svg.append('g')
//			.attr('class', 'axis')
//			.attr('transform', 'translate(0,' + (h - y_pad) + ')')
//			.call(this.xaxis);
//	this.gyaxis = this.svg.append('g')
//			.attr('class', 'axis')
//			.attr('transform', 'translate(' + x_pad + ',0)')
//			.call(this.yaxis);
//	this.addData = function(dat, colour) {
//			if (this.data.length > 0) {
//				this.xmax = Math.max(this.xmax, dat.length);
//				this.ymin = Math.min(this.ymin, Math.min.apply(Math, dat));
//				this.ymax = Math.max(this.ymax, Math.max.apply(Math, dat));
//			} else {
//				this.xmax = dat.length;
//				this.ymin = Math.min.apply(Math, dat);
//				this.ymax = Math.max.apply(Math, dat);
//			}
//			//console.log(this.xmax + ' ' + this.ymin + ' ' + this.ymax);
//			this.xscale.domain([0, this.xmax]);
//			this.yscale.domain([this.ymin, this.ymax]);
//			var that = this;
//			var lineFunc = d3.svg.line()
//					.x(function(d, i) { return that.xscale(i); })
//					.y(function(d) { return that.yscale(d); })
//					.interpolate('linear');
//			var lineFuncZero = d3.svg.line()
//					.x(function(d, i) { return that.xscale(i); })
//					.y(function(d) { return that.yscale(that.ymin); })
//					.interpolate('linear');
//			var line = this.svg.append('svg:path')
//					.attr('stroke', colour)
//					.attr('stroke-width', 2)
//					.attr('fill', 'none')
//					.attr('d', lineFuncZero(dat));
//			this.data.push(dat);
//			this.lines.push(line);
//			for (var i=0; i<this.data.length; i++) {
//				this.lines[i].transition()
//					.delay(1000)
//					.duration(750)
//					.attr('d', lineFunc(this.data[i]));
//			}
//
//			this.gxaxis.call(this.xaxis);
//			this.gyaxis.call(this.yaxis);
//		};
//}
//
